:-dynamic(elemento/1).
:-dynamic(custo/2).
:-dynamic(componente/2).

/*
Especificacao estrutural dos componentes de uma bicicleta tipo.

A bem da simplicidade nao sao considerados diferentes tipos de
bicicletas (estrada, btt, contra-relógio, etc), por outro foram
consideradas simplificacoes na estrutura e detalhe.
*/


% componente(ElementoX,ElementoY,Qtd).
% ElementoX utiliza na sua estrutura o ElementoY na quantiadde Qtd

componente(bicicleta,quadro,1).
componente(bicicleta,roda,2).
componente(bicicleta,conjunto_travagem,1).
componente(bicicleta,conjunto_transmissao,1).
componente(bicicleta,conjunto_selim,1).
componente(bicicleta,direccao,1).
componente(quadro,tubo_superior,1).
componente(quadro,tubo_diagonal,1).
componente(quadro,tubo_selim,1).
componente(quadro,escora_diagonal,1).
componente(quadro,escora_horizontal,1).
componente(quadro,forqueta_frontal,1).
componente(roda,pneu,1).
componente(roda,aro,1).
componente(roda,valvula,1).
componente(roda,cubo,1).
componente(roda,aperto_rapido,1).
componente(roda,raio,30).
componente(conjunto_travagem,travao_direito,1).
componente(conjunto_travagem,travao_esquerdo,1).
componente(travao_esquerdo,manete,1).
componente(travao_esquerdo,cabo,1).
componente(travao_esquerdo,bicha,1).
componente(travao_esquerdo,disco,1).
componente(travao_esquerdo,pastilha,2).
componente(travao_direito,manete,1).
componente(travao_direito,cabo,1).
componente(travao_direito,bicha,1).
componente(travao_direito,disco,1).
componente(travao_direito,pastilha,2).
componente(conjunto_transmissao,pedaleiro,1).
componente(pedaleiro,pedal,1).
componente(pedaleiro,braco_pedal,1).
componente(pedaleiro,rolamento,1).
componente(pedaleiro,prato,1).
componente(conjunto_transmissao,corrente,1).
componente(conjunto_transmissao,desviador_traseiro,1).
componente(conjunto_transmissao,desviador_dianteiro,1).
componente(conjunto_transmissao,cassete,1).
componente(conjunto_transmissao,mudancas_dianteira,1).
componente(mudancas_dianteira,manete_dianteira,1).
componente(mudancas_dianteira,bicha,1).
componente(mudancas_dianteira,cabo,1).
componente(conjunto_transmissao,mudancas_traseira,1).
componente(mudancas_traseira,manete_traseira,1).
componente(mudancas_traseira,bicha,1).
componente(mudancas_traseira,cabo,1).
componente(conjunto_selim,selim,1).
componente(conjunto_selim,espigao,1).
componente(conjunto_selim,aperto_rapido_selim,1).
componente(direccao,caixa_direccao,1).
componente(direccao,guiador,1).
componente(direccao,avanco_guiador,1).
componente(ola,quadro,1).

% ! 2. Escreva o predicado gerar_elemento que cria na base de
% conhecimento factos din�micos do tipo elemento/1 que guardam os
% elementos que existem na �rvore de produto.

gerar_elemento:- (componente(X,_,_), not(elemento(X)), assert(elemento(X))), fail;
                 (componente(_,X,_), not(elemento(X)), assert((elemento(X)))), fail;
                 true.



% ! 3. Escreva o predicado produto_final(Elemento) que permite pesquisar
% elementos que n�o entram na composi��o de outros.

produto_final(Elemento):- setof(X, produto_final_auxiliar(X),R),
                          member(Elemento,R).
 produto_final_auxiliar(Elemento):- componente(Elemento,_,_),
                                   \+(componente(_,Elemento,_)).



% ! 4. Escreva o predicado produto_base(Elemento) que permite pesquisar
% elementos que apenas entram na composi��o de outros.

produto_base(Elemento):-  (setof(X, produto_base_auxiliar(X),R),
                          member(Elemento,R)).
 produto_base_auxiliar(Elemento):- componente(_,Elemento,_),
                                   \+(componente(Elemento,_,_)).



% ! 5. Escreva o predicado produto_intermedio(Elemento) que permite
% pesquisar elementos que entram na composi��o de outros e que s�o
% formados por outros elementos.

produto_intermedio(Elemento):-  setof(X, produto_intermedio_auxiliar(X),R),
                                member(Elemento,R).
 produto_intermedio_auxiliar(Elemento):- componente(Elemento,_,_),
                                         componente(_,Elemento,_).



% ! 6. Escreva o predicado nivel(ElementoX,ElementoY,Nivel) que permite
% determinar a profundidade que o ElementoY est� na �rvore de produto
% relativamente ao ElementoX, considere que a raiz est� no n�vel zero.

nivel(ElementoX,ElementoY,Nivel):- produto_final(Raiz),
                                   nivel_auxiliar(ElementoX,Raiz,Nivel1),
                                   nivel_auxiliar(ElementoY,Raiz,Nivel2),
                                   Nivel is Nivel2-Nivel1,!.
 nivel_auxiliar(ElementoX,ElementoX,0).
 nivel_auxiliar(ElementoX,Raiz,Nivel):- componente(Parent,ElementoX,_),
                                        nivel_auxiliar(Parent,Raiz,Nivel1),
                                        Nivel is Nivel1+1.



% ! 7. Escreva o predicado
% dois_mais_profundos(ElementoRaiz,(EMax1,Prof1),(EMax2,Prof2)) que
% permite determinar os dois elementos, e respectivas profundidades, que
% est�o a maior profundidade na �rvore de produto ElementoRaiz.

descendentes([],[]).
descendentes([X|L], R) :- findall(Y, componente(X, Y, _), L2),
                    descendentes(L2, R1),
                    descendentes(L, R2),
                    append(R1, R2, R3),
                    append([X], R3, R).

primeiro_mais_profundo(Raiz, [X|L], (E1, N1)) :- nivel(Raiz, X, Nivel),
                                                 primeiro_mais_profundo(Raiz, L, (E1, N1), (X, Nivel)).
primeiro_mais_profundo(_, [], (E, Nivel),(E,Nivel)).
primeiro_mais_profundo(Raiz, [Y|L], (E1, N1), (_, Nivel)) :- nivel(Raiz, Y, N2),
                                                             Nivel < N2,
                                                             primeiro_mais_profundo(Raiz, L, (E1, N1), (Y, N2)).
primeiro_mais_profundo(Raiz, [_|L], (E1, N1), (E, Nivel)) :- primeiro_mais_profundo(Raiz, L, (E1, N1), (E, Nivel)).

segundo_mais_profundo(Raiz, L, E1, (E2,N2)) :- delete(L,E1,R), primeiro_mais_profundo(Raiz, R, (E2,N2)), E1 \== E2.

dois_mais_profundos(Raiz,(_,_),(_,_)) :- produto_base(Raiz),
                                         write(Raiz), write(" � elemento base!"),fail.
dois_mais_profundos(Raiz,(E1, N1),(E2, N2)) :- elemento(Raiz),
                                               findall(X, componente(Raiz, X, _), L),
                                               descendentes(L, R),
                                               primeiro_mais_profundo(Raiz, R, (E1, N1)),
                                               segundo_mais_profundo(Raiz, R, E1, (E2,N2)),!.



% ! 8. Escreva o predicado reg_custo(Elemento,Custo) que permite criar
% um facto (din�mico) com o custo de um componente, no caso de j�
% existir um custo para esse componente deve ser atualizado. Assim, a
% chamada do predicado reg_custo(pedal, 32) deve dar origem � cria��o do
% facto custo(pedal, 32) ou � sua atualiza��o.

reg_custo(Elemento,Custo):- elemento(Elemento),
                            retractall(custo(Elemento,_)),
                            assert(custo(Elemento,Custo)).



% ! 9. Escreva o predicado calc_custo(Elemento,Valor,LElemSemCusto) que
% permite calcular o custo de um Elemento em fun��o das quantidades e
% custos dos subprodutos que integram a �rvore do referido elemento; os
% subprodutos que n�o tenham custo definido devem ser coleccionados na
% lista LElemSemCusto.

calc_custo(Elemento,Valor,LElemSemCusto):- elemento(Elemento),
                                           findall(Y, componente(Elemento,Y,_),L),
                                           descendentes(L,R),
                                           calc_custo_auxiliar(R,Valor,LElemSemCusto); true.
 calc_custo_auxiliar([],0,[]).
 calc_custo_auxiliar([H|T],Valor,[H|LElemSemCusto]):- not(custo(H,_)),
                                                   calc_custo_auxiliar(T, Valor,LElemSemCusto).
 calc_custo_auxiliar([H|T],Valor, LElemSemCusto):- custo(H,Valor1),
                                                   calc_custo_auxiliar(T, Soma, LElemSemCusto),
                                                   componente(_,H,L),
                                                   Valor is Soma + Valor1*L.



% ! 10. Escreva o predicado lista_materiais(Elemento,Qtd) que permite
% listar os produtos base e respectivas quantidades e custos envolvidos
% na produ��o de uma Qtd de Elemento por ordem decrescente de custo. A
% estrutura da listagem dever� ser:
%Elemento : Qtd
%-------------------------------------
%produto_base1 , qtd1, custo1
%produto_base2 , qtd2, cust

lista_materiais(Elemento,Qtd):- write("Elemento : Qtd"),nl,
                                write("-------------------------------------"),nl,
                                lista_produto_base(Elemento,L),
                                lista_custo_comp(L,LR),
                                sort(0,@>=,LR,LRC),
                                listar(LRC,Qtd),!.

 lista_produto_base(Elemento,L):- elemento(Elemento),
                                  descendentes_base(Elemento,L).

 descendentes_base(Elemento, R) :- descendentes([Elemento],L),
                                   include(produto_base, L, R).

 lista_custo_comp([],[]).
 lista_custo_comp([H|T],[[(ValorFinal),H]|LR]):- custo(H,Valor1),
                                                 componente(_,H,Q),
                                                 ValorFinal is Valor1*Q,
                                                 lista_custo_comp(T,LR),!.
 lista_custo_comp([H|T],[[0,H]|LR]):- not(custo(H,_)),
                                      lista_custo_comp(T,LR),!.

 listar([],_).
 listar([[Valor1,H]|T], Qtd):- componente(_,H,Q),
                               QT is Q * Qtd,
                               VT is Valor1*Qtd,
                               write("Componente "),write(H),
                               write(", Qtd "),write(QT),
                               write(", Custo "),write(VT),nl,
                               listar(T,Qtd).



% ! 11.Escreva o predicado produto_arvore(Elemento,Arvore) que permite
% construir uma estrutura com a �rvore de produto a partir de do
% Elemento com a seguinte estrutura: ...

produto_arvore(Elemento,[Elemento|Arvore]):- findall(Y, componente(Elemento,Y,_),L),
                                             descendentes_prod_arvore(L,Arvore).

descendentes_prod_arvore([],[]).
descendentes_prod_arvore([H|T], [[H|Arvore2]|Arvore]) :- not(produto_base(H)),
                                                         findall(Y, componente(H, Y, _), L),
					                 descendentes_prod_arvore(T, Arvore),
                                                         descendentes_prod_arvore(L, Arvore2),!.

descendentes_prod_arvore(T, [T]):- verificar_produtos_base(T).

descendentes_prod_arvore([H|T], [H|Arvore]):- descendentes_prod_arvore(T, Arvore).

verificar_produtos_base([]).
verificar_produtos_base([H|T]):- verificar_produtos_base(T),
                                 produto_base(H).



% ! 12. Escreva o predicado
% listar_arvore_identada(Elemento,Op_pi,Op_pb,Op_qtd) que permite listar
% de forma indentada (tipo estrutura de diret�rios) a �rvore de produto
% do Elemento;
% Op_pi define se s�o apresentados os produtos interm�dios (on/off);
% Op_pb define se s�o apresentados os produtos base (on/off);
% Op_qtd define se s�o apresentados as quantidades (on/off).
% 0: off    1:on

listar_arvore_identada(Elemento,Op_Pi, Op_Pb,Op_Qtd):- produto_arvore(Elemento,Arvore),
                                                       flatten(Arvore,ArvoreFlatten),
                                                       lista(ArvoreFlatten,Op_Pi,Op_Pb,ArvoreComSemPiPb),
                                                       listar_identada(Elemento,ArvoreComSemPiPb,Op_Qtd),!.

 lista(L,1,1,L).
 lista(L,0,0,LR):- exclude(produto_intermedio,L,LA), exclude(produto_base,LA,LR).
 lista(L,0,1,LR):- exclude(produto_intermedio,L,LR).
 lista(L,1,0,LR):- exclude(produto_base,L,LR).

 listar_identada(_,[],_).
 listar_identada(Elemento,[H|T],Qtd):- nivel(Elemento,H,N),
                                       writeQtd(N, H, Qtd),
                                       listar_identada(Elemento,T,Qtd).

 writeQtd(N,H,1):- spacement(N), write(H), write("     Qtd : "), (componente(H,_,Q);componente(_,H,Q)), write(Q), nl.
 writeQtd(N,H,0):- spacement(N), write(H), nl.
 spacement(N):- E is N*5, tab(E).



% ! 13. Escreva o predicado guardarBaseConhecimento(Nome) que permite
% guardar a base de conhecimento num ficheiro de texto (incluindo os
% predicados criados dinamicamente).

guardarBaseConhecimento(Nome):- tell(Nome), listing, told.






